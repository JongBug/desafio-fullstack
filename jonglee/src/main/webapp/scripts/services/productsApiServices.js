app.service("productsApi", function($http, config, $window){
	return{
		getList: function(){
			return $http.get( config.baseUrl + "/allProducts")
			.then(function(result){
				return result.data;
			},
       function(result){
        console.log(result);
       });
		},
		addPolicy: function(product){
			return $http.post( config.baseUrl + "/addProduct", product)
			.then(function(result){
				return result.status;
			},
       function(result){
        console.log(result);
       });
		}
	};
});
