window.fbAsyncInit = function() {
   FB.init({
     appId      : '1728425060798719',
     cookie     : true,
     xfbml      : true,
     version    : 'v2.10'
   });

   FB.AppEvents.logPageView();
};

// Load the SDK asynchronously
(function(d, s, id) {
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) return;
 js = d.createElement(s); js.id = id;
 js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=1728425060798719';
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
