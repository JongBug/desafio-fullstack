app.controller("cartCtrl", function($scope, config, $window) {

	$scope.cartListView = config.cartList

	$scope.removerCarrinho = function(id) {
		console.log(id);
		config.cartList.splice(id, 1);
	};

	$scope.valorTotal = function() {
		$scope.fatorA = 0;
		$scope.valorA = 0;
		$scope.fatorB = 0;
		$scope.valorB = 0;
		$scope.fatorC = 0;
		$scope.valorC = 0;
		$scope.porcA = 0;
		$scope.porcB = 0;
		$scope.porcC = 0;

		for (i = 0; i < config.cartList.length; i++) {
			if (config.cartList[i].fator === "A") {
				$scope.fatorA++;
				$scope.valorA += config.cartList[i].unit_price;
			} else if (config.cartList[i].fator == "B") {
				$scope.fatorB++;
				$scope.valorB += config.cartList[i].unit_price;
			} else {
				$scope.fatorC++;
				$scope.valorC += config.cartList[i].unit_price;
			}
		}
		
		//Regra fator A
		if($scope.fatorA <= 1){
			$scope.valorATotal = $scope.valorA;
		} else if ($scope.fatorA <= 5) {
			$scope.valorATotal = ($scope.valorA * (1 - ($scope.fatorA / 100)))
			$scope.porcA = $scope.fatorA * 1
		} else {
			$scope.porcA = 5
			$scope.valorATotal = ($scope.valorA * (1 - 0.05))
		}
		
		//Regra fator B
		if($scope.fatorB <= 1){
			$scope.valorBTotal = $scope.valorB;
		} else if ($scope.fatorB <= 3) {
			$scope.porcB = $scope.fatorB * 5
			$scope.valorBTotal = ($scope.valorB * (1 - ($scope.fatorB / 100)))
		} else {
			$scope.porcB = 15
			$scope.valorBTotal = ($scope.valorB * (1 - 0.15))
		}
		//Regra fator C
		if($scope.fatorC <= 1){
			$scope.valorCTotal = $scope.valorC;
		} else if ($scope.fatorC <= 3) {
			$scope.valorCTotal = ($scope.valorC * (1 - ($scope.fatorC / 100)))
			$scope.porcC = $scope.fatorC * 10
		} else {
			$scope.porcC = 30
			$scope.valorCTotal = ($scope.valorC * (1 - 0.3))
		}
		
		$scope.valorTotalSem = ($scope.valorA + $scope.valorB + $scope.valorC);
		$scope.valorTotalCom = ($scope.valorATotal + $scope.valorBTotal + $scope.valorCTotal);
		
		$scope.valorFinal=0;
		
		if(($scope.valorTotalSem * (1-0.3))>= $scope.valorTotalCom){
			$scope.valorFinal = ($scope.valorTotalSem * (1-0.3))
		}else {
			$scope.valorFinal = $scope.valorTotalCom;
		}
		
		
		config.valorFinal = $scope.valorFinal
		
	}
});