app.controller("productsCtrl", function($scope, productsApi, config, $http){
	
	//GET method responsible for get all products data
  $scope.getProductList = function() {
	  productsApi.getList().then(function (data) {
      $scope.productsList = data;
      }, function (error) {
      console.log('Unable to load contacts data: ' + error.message);
    });
  };
  
  $scope.itensCarrinho = [];
  $scope.adicionarCarrinho = function (data) {
	  config.cartList.push(data);
	  alert("Adicionado no Carrinho");
  }

  $scope.fatorValor = config.fatorValor;
  
  $scope.adicionarProduto = function () {
	    if($scope.ngName!=null && $scope.ngDescription!=null  && $scope.ngValue!=null  && $scope.ngFator!=null ){
	    	
	  	  var dataObj = {
			      title : $scope.ngName,
			      description : $scope.ngDescription,
			      unit_price : $scope.ngValue,
			      fator : $scope.ngFator
			    };
	  	  
	      $http.post('totvs/jonglee/addProduct', dataObj).then(function(success) {
	    	  
		        $('#modalAdd').modal('hide');
		        alert("Adicionado com Sucesso");
		        location.reload();
		        
	      }, function (response) {       
            	alert("Valores invalidos");
            }); 
	    } else {
	    	alert("Valores invalidos");
	    }
  }
  
});
