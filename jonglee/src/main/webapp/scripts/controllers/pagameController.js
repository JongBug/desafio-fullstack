app.controller("pagameCtrl", function($scope, pagameApi, config, $window){
	
$scope.jsonData = {
	    "api_key": "ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd",
	    "amount": 21000,
	    "card_number": "4111111111111111",
	    "card_cvv": "123",
	    "card_expiration_date": "0922",
	    "card_holder_name": "João das Neves",
	    "customer": {
	      "external_id": "#3311",
	      "name": "João das Neves Braulio",
	      "type": "individual",
	      "country": "br",
	      "email": "joaodasneves@got.com",
	      "documents": [
	        {
	          "type": "cpf",
	          "number": "00000000000"
	        }
	      ],
	      "phone_numbers": ["+5511999998888", "+5511888889999"],
	      "birthday": "1965-01-01"
	    },
	    "billing": {
	      "name": "João das Neves",
	      "address": {
	        "country": "br",
	        "state": "sp",
	        "city": "Cotia",
	        "neighborhood": "Rio Cotia",
	        "street": "Rua Matrix",
	        "street_number": "9999",
	        "zipcode": "06714360"
	      }
	    },
	    "shipping": {
	      "name": "Neo Reeves",
	      "fee": 1000,
	      "delivery_date": "2000-12-21",
	      "expedited": true,
	      "address": {
	        "country": "br",
	        "state": "sp",
	        "city": "Cotia",
	        "neighborhood": "Rio Cotia",
	        "street": "Rua Matrix",
	        "street_number": "9999",
	        "zipcode": "06714360"
	      }
	    },
	    "items": config.pagameItem
	};

  $scope.submit = function (){
	  for (i = 0; i < config.cartList.length; i++) { 
	  	config.pagameItem.push({
	  		id: config.cartList[i].id,
	  		title: config.cartList[i].title,
	  		unit_price: parseInt(config.cartList[i].unit_price*100),
	  		quantity: 1,
	  		tangible:true
		});
	}
	  
	  pagameApi.postItens($scope.jsonData)
	  .then(function(data){
	    if(data==200){
	      alert("Pagamento efetuado com sucesso!");
	      $window.location.href = './main.html';
	    }
	  });
  }
  
  $scope.resumeList = config.cartList;
  
	$scope.valorTotal = function() {
		$scope.fatorA = 0;
		$scope.valorA = 0;
		$scope.fatorB = 0;
		$scope.valorB = 0;
		$scope.fatorC = 0;
		$scope.valorC = 0;
		$scope.porcA = 0;
		$scope.porcB = 0;
		$scope.porcC = 0;

		for (i = 0; i < config.cartList.length; i++) {
			if (config.cartList[i].fator === "A") {
				$scope.fatorA++;
				$scope.valorA += config.cartList[i].unit_price;
			} else if (config.cartList[i].fator == "B") {
				$scope.fatorB++;
				$scope.valorB += config.cartList[i].unit_price;
			} else {
				$scope.fatorC++;
				$scope.valorC += config.cartList[i].unit_price;
			}
		}
		
		//Regra fator A
		if($scope.fatorA <= 1){
			$scope.valorATotal = $scope.valorA;
		} else if ($scope.fatorA <= 5) {
			$scope.valorATotal = ($scope.valorA * (1 - ($scope.fatorA / 100)))
			$scope.porcA = $scope.fatorA * 1
		} else {
			$scope.porcA = 5
			$scope.valorATotal = ($scope.valorA * (1 - 0.05))
		}
		
		//Regra fator B
		if($scope.fatorB <= 1){
			$scope.valorBTotal = $scope.valorB;
		} else if ($scope.fatorB <= 3) {
			$scope.porcB = $scope.fatorB * 5
			$scope.valorBTotal = ($scope.valorB * (1 - ($scope.fatorB / 100)))
		} else {
			$scope.porcB = 15
			$scope.valorBTotal = ($scope.valorB * (1 - 0.15))
		}
		//Regra fator C
		if($scope.fatorC <= 1){
			$scope.valorCTotal = $scope.valorC;
		} else if ($scope.fatorC <= 3) {
			$scope.valorCTotal = ($scope.valorC * (1 - ($scope.fatorC / 100)))
			$scope.porcC = $scope.fatorC * 10
		} else {
			$scope.porcC = 30
			$scope.valorCTotal = ($scope.valorC * (1 - 0.3))
		}
		
		$scope.valorTotalSem = ($scope.valorA + $scope.valorB + $scope.valorC);
		$scope.valorTotalCom = ($scope.valorATotal + $scope.valorBTotal + $scope.valorCTotal);
		
		$scope.valorFinal=0;
		
		if(($scope.valorTotalSem * (1-0.3))>= $scope.valorTotalCom){
			$scope.valorFinal = ($scope.valorTotalSem * (1-0.3))
		}else {
			$scope.valorFinal = $scope.valorTotalCom;
		}
		
		
		config.valorFinal = $scope.valorFinal
		
	}
});
