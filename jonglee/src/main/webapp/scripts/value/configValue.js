app.value("config",{
	baseUrl : "http://localhost:8080/jonglee/totvs/jonglee",
	routeList: [
  	{ name : "Produtos", url: "products"},
  	{ name : "Meu Carrinho de Compras", url: "cart"},
    { name : "Resumo da Compra", url: "resume"}
	],
	cartList: [],
	pagameItem: [],
	valorFinal: 0,
	fatorValor: ["A", "B", "C"]
});
