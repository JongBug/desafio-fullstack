package br.com.totvs.jonglee.dao;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import br.com.totvs.jonglee.dao.imageReader;
import br.com.totvs.jonglee.beans.ProductsData;

/**
 * Class responsible for SQL Commands
 * @author jong.lee
 * @version 1.0
 */
public class SqlCommands extends ConnectionToSQLite{
	
	public static SqlCommands instance;
	
	public static SqlCommands getInstance(){
		synchronized (SqlCommands.class) {
			if (instance == null){
				instance = new SqlCommands();
			}
		}
		return instance;	
	}
	
	/**
	 * This method list all products contain in data base
	 */
	public ArrayList<ProductsData> getAllContent(){
		
		ArrayList<ProductsData> listAll = new ArrayList<ProductsData>();
		
		Connection connect =null;
		PreparedStatement pstmt  = null;
		ResultSet rs = null;
		FileOutputStream fos = null;
		
		connect = getConnection();

		try {
			pstmt = connect.prepareStatement("SELECT * FROM PRODUCTS");
			rs = pstmt.executeQuery();
			  
			while(rs.next()){
				ProductsData dados = new ProductsData();
				dados.setId(rs.getString("products_id"));
				dados.setTitle(rs.getString("products_name"));
				dados.setDescription(rs.getString("products_description"));
				
//                InputStream input = rs.getBinaryStream("products_image");
//                byte[] buffer = new byte[1024];
//                while (input.read(buffer) > 0) {
//                    fos.write(buffer);
//                }
//                
				//dados.setImage();
				dados.setUnit_price(rs.getFloat("products_value"));
				dados.setFator(rs.getString("products_fator"));
				listAll.add(dados);
			}	
		} catch (Exception e) {
			System.out.println("Erro ao selecionar o BD"+e);
		}finally {
			closeConnection(connect, pstmt, rs);
		}
		
		return listAll;
	}
	
	/**
	 * This method add a product in data base
	 */
	public void addInDB(ProductsData product){
		imageReader reader = new imageReader();
		PreparedStatement pstmt  = null;
		try (Connection connect = getConnection()){
			pstmt = connect.prepareStatement("INSERT INTO PRODUCTS (products_name, products_description, products_value, products_fator) VALUES (?, ?, ?, ?)");
			pstmt.setString(1, product.getTitle());
			pstmt.setString(2, product.getDescription());
//			pstmt.setBytes(3, reader.readFile(product.getImage()));
			pstmt.setFloat(3, product.getUnit_price());
			pstmt.setString(4, product.getFator());
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Erro ao adicionar no BD:" + e);
		}
	}
}
